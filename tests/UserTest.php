<?php

declare(strict_types = 1);

namespace Tests;

//use App\Models\FileSystem;
use App\Models\FileSystem;
use App\Models\SomeClass;
use App\Models\User;
use ArgumentCountError;
use BadFunctionCallException;
use BadMethodCallException;
use InvalidArgumentException;
use PHPUnit\Framework\Error\Error;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest
 */
class UserTest extends TestCase
{
    /**
     * @return User
     */
    public function test_add_raiting()
    {
        $this->assertTrue(true);
        $this->assertEquals(true, true);
        $user = new User();
        $result = $user->add_raiting(10);
        $this->assertEquals($result, 123);

        return $user;
    }

    /**
     * @test
     * @return string
     */
    public function two()
    {
        $this->assertTrue(true);
        return 'two';
    }

    /**
     * @return string
     *
     */
    public function testOne()
    {
        $this->assertTrue(2>1, 'message');

        return 'one';
    }

    /**
     * @depends testOne
     * @depends two
     *
     * @param $one
     * @param $two
     */
    public function testResult($one, $two)
    {
        $this->assertSame('one', $one);
        $this->assertEquals('two', $two);
    }

    public function additionalProvider()
    {
        return [
            'adding zeros' => [0, 0, 0],
            'zero plus one' => [0, 1, 1],
            'one plus zero' => [1, 0, 1],
            'one plus one' => [1, 1, 2],
        ];
    }

    /**
     * @dataProvider additionalProvider
     * @depends      testOne
     *
     * @param $a
     * @param $b
     * @param $result
     * @param $one
     *
     * @return string
     */
    public function testAdd($a, $b, $result, $one): string
    {
        $this->assertSame('one', $one);
        self::assertEquals($result, $a + $b);

        return $one;
    }

    /**
     * @depends test_add_raiting
     *
     * @param $user
     */
    public function testException($user)
    {
        $this->assertEquals([1,2,'3',4,5],[1,2,3,4,5]);
    }

    public function testMockObjects()
    {
        $user = $this->createMock(User::class);
        $user->method('add_raiting')->willReturn('100');

        $this->assertSame('100', $user->add_raiting(10));

    }

    public function testReturnArgumentStub()
    {
        // Создать заглушку для класса SomeClass.
        $stub = $this->createMock(User::class);

        // Настроить заглушку.
        $stub->method('add_raiting')
            ->will($this->returnArgument(1));

        // $stub->doSomething('foo') вернёт 'foo'
        $this->assertSame(true, $stub->add_raiting(1));

        // $stub->doSomething('bar') вернёт 'bar'
        $this->assertEquals(2, $stub->add_raiting(2));
    }

    public function testFetchesData()
    {
        $file = $this->createMock(FileSystem::class);
        $file->method('get')
            ->willReturn('foo');
        $someClass = new SomeClass($file);
        $data = $someClass->fetch('string');
        $this->assertEquals('foo', $data);
    }


}
