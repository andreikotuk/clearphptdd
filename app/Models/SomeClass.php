<?php

declare(strict_types = 1);

namespace App\Models;

class SomeClass
{
    public $fileSystem;
    public function __construct(FileSystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    public function fetch(string $str)
    {
        return $this->fileSystem->get($str);
    }
}
